#define CLIENT_SSL_ENABLE
#define UPGRADE_SSL_ENABLE

#include <SPI.h>
#include <Wire.h>
#include <EEPROM.h>
#include <MFRC522.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <xxtea-iot-crypt.h>

/* MF RC522 RFID configuration */
#define RST_PIN 2 // RST-PIN for RC522 - RFID - SPI 
#define SS_PIN  0 // SDA-PIN for RC522 - RFID - SPI

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance

/* External device pins configuration */
//#define LED_PIN 15
//#define RELAY_PIN 16
#define CONFIG_RELAY_PIN 16 // if HIGH on boot, enter config mode

/* OLED configuration */
#define OLED_LINE_LEN 20
#define OLED_RESET LED_BUILTIN
Adafruit_SSD1306 oled(OLED_RESET);

/* Configuration structure which gets stored in the simulated EEPROM */
#define CONFIG_SIZE_MAX 1024
#define CONFIG_SIGNATURE 0x12
#define CONFIG_VERSION 2
#define SSID_MAX_LEN 32
#define PASS_MAX_LEN 32
#define URL_MAX_LEN 128
struct device_config {
  byte  signature;
  byte  version;
  char  config_wifi_ssid[SSID_MAX_LEN];
  char  config_wifi_pass[SSID_MAX_LEN];
  char  wifi_ssid[SSID_MAX_LEN];
  char  wifi_pass[PASS_MAX_LEN];
  char  api_url[URL_MAX_LEN];
  char  config_url[URL_MAX_LEN];
};

struct device_config cfg;

/* Current device mode */
#define MODE_INVALID 0
#define MODE_CONFIG 1
#define MODE_RUNNING 2
byte mode = MODE_INVALID;

/* WiFi-related configuration */
#define DEFAULT_WIFI_CONFIG_AP_SSID "RFIDCFG"
#define DEFAULT_WIFI_CONFIG_AP_PASS "cfg"

ESP8266WebServer webserver(80);

/* Various timers, for timeouts on API, relays and messages */
uint32_t last_api_send_millis = 0;
#define RELAY_ON_TIME 5000
uint32_t last_relay_on_millis = 0;
#define XMSG_ON_TIME 3000
uint32_t last_xmsg_millis = 0;

/* ******************************************************************** EEPROM configuration functions */

void read_eeprom_config(struct device_config *c) {
  byte *buf = (byte*) c;
  for (int i = 0; i < sizeof(*c); i++) {
    *(buf++) = EEPROM.read(i);
  }
  if (c->signature != CONFIG_SIGNATURE || c->version != CONFIG_VERSION) {
    memset(c, 0, sizeof(*c));
    // Initialize some defaults
    byte mac[6];
    WiFi.macAddress(mac);

    byte keybuf[] = "Random Password!";
    if (xxtea_setup(keybuf, strlen((char*)keybuf)) != XXTEA_STATUS_SUCCESS) {
      Serial.println("XXTEA key barfed");
    }

    byte ebuf[16];
    size_t ebuf_len = 16;
    if (xxtea_encrypt(mac, sizeof(mac), ebuf, &ebuf_len) != XXTEA_STATUS_SUCCESS) {
      Serial.println("XXTEA encrypt barfed");
    }
    
    strcpy(cfg.config_wifi_ssid, DEFAULT_WIFI_CONFIG_AP_SSID);
    strcat(cfg.config_wifi_ssid, "-");
    for (int i = 0; i < 3; i++) {
      char buf[3];
      sprintf(buf, "%02x", ebuf[i]);
      strcat(cfg.config_wifi_ssid, buf);
    }
    strcpy(cfg.config_wifi_pass, DEFAULT_WIFI_CONFIG_AP_PASS);
    for (int i = 3; i < 6; i++) {
      char buf[3];
      sprintf(buf, "%x", ebuf[i]);
      strcat(cfg.config_wifi_pass, buf);
    }
  }
}

void write_eeprom_config(struct device_config *c) {
  c->signature = CONFIG_SIGNATURE;
  c->version = CONFIG_VERSION;
  byte *buf = (byte*) c;
  for (int i = 0; i < sizeof(*c); i++) {
    EEPROM.write(i, *(buf++));
  }
  EEPROM.commit();
}

/* ******************************************************************** External device functions */

void turn_led_on() {
  //digitalWrite(LED_PIN, HIGH);
}

void turn_led_off() {
  //digitalWrite(LED_PIN, LOW);
}

void turn_relay_on() {
  digitalWrite(CONFIG_RELAY_PIN, HIGH);
}

void turn_relay_off() {
  digitalWrite(CONFIG_RELAY_PIN, LOW);
}

/* ******************************************************************** OLED support functions */

void clear_oled_line(byte y) {
  char line[OLED_LINE_LEN+1];
  memset(line, ' ', OLED_LINE_LEN);
  line[OLED_LINE_LEN] = '\0';
  
  int16_t oldX = oled.getCursorX();
  int16_t oldY = oled.getCursorY();
  oled.setCursor(0, y);
  oled.setTextColor(WHITE, BLACK);
  oled.print(line);
  oled.setCursor(oldX, oldY);
}

void show_oled_spinner(byte y, char dot = '.') {  
  char spline[OLED_LINE_LEN+1];
  memset(spline, ' ', OLED_LINE_LEN);
  spline[OLED_LINE_LEN] = '\0';
  
  static signed char dot_pos = 0;
  static byte dot_direction = 0;
  static signed char dot_y = -1;

  if (dot_y != y) {
    dot_y = y;
    dot_pos = 0;
  }
  spline[dot_pos] = dot;

  if (dot_direction == 0) {
    dot_pos++;
    if (dot_pos >= OLED_LINE_LEN) {
      dot_direction = 1;
      dot_pos--;
    }
  } else {
    dot_pos--;
    if (dot_pos <= 0) {
      dot_direction = 0;
      dot_pos++;
    }
  }
  int16_t oldX = oled.getCursorX();
  int16_t oldY = oled.getCursorY();
  oled.setCursor(0, y);
  oled.setTextColor(WHITE, BLACK);
  oled.print(spline);
  oled.display();
  oled.setCursor(oldX, oldY);
}

/* ******************************************************************** Web server support functions */

void config_web_server() {
  webserver.on("/", http_root);
  webserver.on("/savesettings", http_save_settings);
  webserver.begin();
}

// Runs the configuration access point - with a default username and password 
void config_ap_web_server() {
  turn_led_on();
  
  //oled.println("Configuration mode");
  oled.display();
  
  WiFi.softAP(cfg.config_wifi_ssid, cfg.config_wifi_pass);
  IPAddress a = WiFi.softAPIP();

  oled.print("SSID: ");
  oled.println(cfg.config_wifi_ssid);
  oled.print("pass: ");
  oled.println(cfg.config_wifi_pass);
  oled.println(a);

  config_web_server();

  oled.display();
  
  mode = MODE_CONFIG;
  /*
  Serial.println("AP web server configuration mode");
  Serial.println(a);
  */
}

void config_plain_web_server() {
  turn_led_off();
  oled.clearDisplay();
  oled.setTextColor(WHITE, BLACK);
  oled.setCursor(0, 0);
  oled.print("WiFi: ");
  oled.println(cfg.wifi_ssid);
  oled.display();
  WiFi.begin(cfg.wifi_ssid, cfg.wifi_pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    show_oled_spinner(1*8, '?');
  }
  oled.print("IP: ");
  IPAddress ip = WiFi.localIP();
  oled.println(String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]));
  
  config_web_server();

  oled.display();
  
  mode = MODE_RUNNING;
}

/* ******************************************************************** Remote API functions */

void send_api_uid(byte *buf, byte len) {

  if (millis() - last_api_send_millis < 2000)
    return;
  
  String hex_uid;
  for (byte i = 0; i < len; i++) {
    if (buf[i] < 0x10)
      hex_uid += '0';
    hex_uid += String(buf[i], HEX);
  }
  String url(cfg.api_url);
  url.replace("%u", hex_uid);
  Serial.println(url);
  
  HTTPClient http;
  http.begin(url);
  int http_code = http.GET();
  if (http_code > 0) {
    Serial.println(http_code);
    if (http_code == HTTP_CODE_OK) {
      String response = http.getString();
      oled.setCursor(0, 3*8);
      oled.print(response);
      oled.display();
      int p = response.indexOf('\n');
      String card_ok = response.substring(0, p);
      
      int p1 = response.indexOf('\n', p+1);
      String door_ok = response.substring(p+1, p1);
      
      String msg = response.substring(p1+1, response.length()-1);

      Serial.println("CARD: '"+card_ok+"' door: '"+door_ok+"' msg: '"+msg+"'");
      if (door_ok == "OK") {
        turn_relay_on();
        last_relay_on_millis = millis();
      }
      last_xmsg_millis = millis();
    }
  }
  http.end();

  last_api_send_millis = millis();
}

/* ******************************************************************** Debugging functions */

void dump_byte_array(byte *buffer, byte bufferSize, Print &prn) {
  for (byte i = 0; i < bufferSize; i++) {
    prn.print(buffer[i] < 0x10 ? " 0" : " ");
    prn.print(buffer[i], HEX);
  }
}

void dump_card_data(Print &prn) {
  prn.print(F("Card UID:"));
  dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size, prn);
  prn.println();
}

/* ******************************************************************** Arduino setup() */

void setup() {
  //pinMode(LED_PIN, OUTPUT);
  //turn_led_off();

  pinMode(CONFIG_RELAY_PIN, INPUT_PULLUP);
  Serial.begin(9600);    // Initialize serial communications

  oled.begin(SSD1306_SWITCHCAPVCC, 0x3c);  // initialize with the I2C addr 
  oled.clearDisplay();

  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.setCursor(0, 0);
  oled.println("Initialising...");
  oled.display();
  
  SPI.begin();           // Init SPI bus
  mfrc522.PCD_Init();    // Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details

  byte v = mfrc522.PCD_ReadRegister(MFRC522::VersionReg);
  if (v == 0 || v == 0xff) {
    oled.setTextColor(BLACK, WHITE);
    oled.println("Error in RC522 RFID");
    oled.setTextColor(WHITE, BLACK);
  } else {
    oled.print("RC522 RFID ver");
    switch(v) {
      case 0x88: oled.println(F(" = (clone)"));  break;
      case 0x90: oled.println(F(" = v0.0"));     break;
      case 0x91: oled.println(F(" = v1.0"));     break;
      case 0x92: oled.println(F(" = v2.0"));     break;
      default:   oled.println(F(" = (unknown)"));
    }
  }

  EEPROM.begin(CONFIG_SIZE_MAX);
  oled.print("Reset button?");
  oled.display();  
  bool button = false;
  for (int i = 0; i < 200; i++) {
    if (digitalRead(CONFIG_RELAY_PIN) == LOW) {
      button = true;
      break;
    }
    delay(10);
  }

  if (button) {
    Serial.println("YES!");
  } else {
    oled.println(" no.");
  }

  read_eeprom_config(&cfg);
  if (button || strlen(cfg.wifi_ssid) == 0 || strlen(cfg.api_url) == 0) {
    config_ap_web_server();
  } else {
    config_plain_web_server();
  }
}

/* ******************************************************************** Arduino loop() */

void loop() { 
  if (mode == MODE_CONFIG || mode == MODE_RUNNING) {
    webserver.handleClient();
  }

  if (mode == MODE_CONFIG) {
    oled.setCursor(0, 0);
    IPAddress ip = WiFi.softAPIP();
    oled.println(String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]) + "      ");
  }

  if (last_relay_on_millis != 0) {
    if (millis() - last_relay_on_millis > RELAY_ON_TIME) {
      turn_relay_off();
      last_relay_on_millis = 0;
    }
  }

  if (last_xmsg_millis != 0) {
    if (millis() - last_xmsg_millis > XMSG_ON_TIME) {
      clear_oled_line(3*8);
      clear_oled_line(4*8);
      clear_oled_line(5*8);
      oled.display();
      last_xmsg_millis = 0;
    }
  }

  yield();
  // Look for new cards. The rest of the code is only executed when there's a card present.
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    show_oled_spinner(6*8);
    delay(50);
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    delay(50);
    return;
  }
  // Show some details of the PICC (that is: the tag/card)
  dump_card_data(Serial);

  oled.setTextColor(WHITE, BLACK);
  oled.setCursor(0, 7*8);
  dump_card_data(oled);
  oled.display();

  if (cfg.api_url[0] != '\0') {
    send_api_uid(mfrc522.uid.uidByte, mfrc522.uid.size);
  }  
}


/* ******************************************************************** HTML */

String get_html_head(String title) {
  return "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n<meta charset=\"utf-8\">\n<meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n<title>" + title + "</title>\n" \
  "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" \
  "<style>\n" \
  " body { font-family: sans-serif; max-width: 1000px; margin-left: auto; margin-right: auto; }\n" \
  "</style>\n" \
  "</head>\n<body>\n";
}

String get_html_foot() {
  return "</body>\n</html>\n";
}

/* ******************************************************************** Web server routes / HTTP handlers */

void http_root() {
  String html = get_html_head("WiFi RFID setup");
  html += "<h1>RFID device configuration</h1>\n";
  html += "<p>Here you can configure some RFID card reader settings. If you don't know what you're doing, consult an expert.</p>\n";
  html += "<form method=\"POST\" action=\"/savesettings\">\n";
  html += "<table align=\"center\"><tr><td>WiFi SSID to connect to:</td><td><input name=\"ssid\" type=\"text\"";
  if (cfg.wifi_ssid[0] != '\0')
    html += " value=\"" + String(cfg.wifi_ssid) + "\"";
  html += "></td></tr>\n";
  html += "<tr><td>WiFi password:</td><td><input name=\"pass\" type=\"password\"></td></tr>\n";
  html += "<tr><td>RFID API URL:</td><td><input name=\"api_url\" type=\"text\" size=\"50\"";
  if (cfg.api_url[0] != '\0')
    html += " value=\"" + String(cfg.api_url) + "\"";
  html += "></td></tr>\n";
  html += "<tr><td>Config API URL:</td><td><input name=\"config_url\" type=\"text\" size=\"50\"";
  if (cfg.config_url[0] != '\0')
    html += " value=\"" + String(cfg.config_url) + "\"";
  html += "></td></tr>\n";
  html += "<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\"></td></tr>\n";
  html += get_html_foot();
  webserver.send(200, "text/html", html);
}

void http_save_settings() {
  String html = get_html_head("WiFi RFID setup");
  html += "<p>Configuring for SSID: \"" + webserver.arg("ssid") + "\" and API URL: \"" + webserver.arg("api_url") + "\".</p>\n";

  strlcpy(cfg.wifi_ssid, webserver.arg("ssid").c_str(), sizeof(cfg.wifi_ssid));
  if (webserver.arg("pass").length() != 0)
    strlcpy(cfg.wifi_pass, webserver.arg("pass").c_str(), sizeof(cfg.wifi_pass));
  strlcpy(cfg.api_url, webserver.arg("api_url").c_str(), sizeof(cfg.api_url));
  strlcpy(cfg.config_url, webserver.arg("config_url").c_str(), sizeof(cfg.config_url));
  write_eeprom_config(&cfg);

  html += "<p>The device will now reboot.</p>";
  
  html += get_html_foot();
  webserver.send(200, "text/html", html);
  delay(1000);
  ESP.restart();
}

