<?php
/*
 * Expecting GET arguments:
 * uid  -   a hex string of UID bytes
 * ver  -   expected API version
 */

if (!isset($_GET['uid']))
    die("ERROR\nNo UID");

$uid = $_GET['uid'];
if ($uid[0] == '5') {
    $pass = true;
    $door = true;
    $msg = "Welcome!";
} else {
    $pass = false;
    $door = false;
    $msg = "GO AWAY!";
}

header("Content-type: text/plain");
echo $pass ? "OK\n" : "NO\n";
echo $door ? "OK\n" : "NO\n";
echo "$msg\n";

