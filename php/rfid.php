<?php
/*
 * Example integration with Odoo 10
 *                                       Table "public.hr_attendance"
    Column    |            Type             |                         Modifiers
--------------+-----------------------------+------------------------------------------------------------
 id           | integer                     | not null default nextval('hr_attendance_id_seq'::regclass)
 create_uid   | integer                     |
 check_in     | timestamp without time zone | not null
 employee_id  | integer                     | not null
 worked_hours | double precision            |
 write_uid    | integer                     |
 create_date  | timestamp without time zone |
 write_date   | timestamp without time zone |
 check_out    | timestamp without time zone |
 *
 * Expecting GET arguments:
 * uid  -   a hex string of UID bytes
 * ver  -   expected API version
 */

if (php_sapi_name() == 'cli') {
    $uid = pg_escape_string(trim(file_get_contents('php://stdin')));
} else {
    $uid = pg_escape_string($_GET['uid']);
}

if (!isset($uid) || empty($uid))
    die("ERROR\nNo UID");

$db = pg_connect('dbname=signalgrad user=odoo');
if (!$db)
    blowup("Db connection failed");

pg_query("SET timezone='UTC'");

$res = pg_query($db, "SELECT id, write_uid, name FROM hr_employee WHERE barcode='$uid'");
if (!$res)
    blowup("employee db query failed");

$resp = array('ok' => false, 'door' => false, 'msg' => 'Error', 'name' => '');

$row = pg_fetch_array($res);
if ($row) {
    $resp['door'] = true;
    $resp['name'] = $row['name'];

    $employee_id = $row['id'];
    $write_id = $row['write_uid'];
    // Update attendance record
    // Step 1: Find if there's an existing, unfinished time record
    $res = pg_query($db, "SELECT id, check_in, create_date, write_date FROM hr_attendance WHERE employee_id=$employee_id AND check_out IS NULL");
    if (!$res)
        blowup("hr_attendance db query failed");
    $row = pg_fetch_array($res);
    if ($row) {
        // Step 2: There's a checkin record. Update it to a checkout record.
        $res = pg_query("UPDATE hr_attendance SET check_out=NOW(), write_date=NOW(), worked_hours=abs(extract(epoch from now() - check_in)/3600.0) WHERE id={$row['id']}");
        if (!$res)
            blowup("update hr_attendance query failed");
        $resp['msg'] = 'CHECKOUT';
    } else {
        // Step 3: Create a new checkin record
        $res = pg_query("INSERT INTO hr_attendance(check_in, employee_id, worked_hours, create_date, write_date) VALUES (now(), $employee_id, 0, now(), now())");
        if (!$res)
            blowup("insert hr_attendance query failed");
        $resp['msg'] = 'CHECKIN';
    }
    $resp['ok'] = true;
} else {
    $resp['msg'] = 'GO AWAY!';
}

header("Content-type: application/json");
echo json_encode($resp);


function blowup($msg) {
    header("HTTP/1.0 500 Error: $msg");
    header('Content-type: text/plain');
    file_put_contents('/tmp/rfid.php.log', "$msg\n", FILE_APPEND);
    $resp['ok'] = false;
    $resp['msg'] = $msg;
    echo json_encode($resp);
    exit;
}
