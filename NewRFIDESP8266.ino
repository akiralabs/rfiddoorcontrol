// Sketch for board "NodeMCU 1.0 (ESP-12)"
// Using LiquidCrystal library from https://github.com/agnunez/ESP8266-I2C-LCD1602

#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <xxtea-iot-crypt.h>
#include <ArduinoJson.h>

const byte PIN_BUZZER = 13;
const byte PIN_RELAY = 14;
const byte PIN_LED = LED_BUILTIN;
const byte PIN_BUTTON = 12;
const int RELAY_TIME = 4000; // milliseconds

/* Configuration structure which gets stored in the simulated EEPROM */
#define CONFIG_SIZE_MAX 1024
#define CONFIG_SIGNATURE 0x12
#define CONFIG_VERSION 1
#define SSID_MAX_LEN 32
#define PASS_MAX_LEN 32
#define URL_MAX_LEN 128
struct device_config {
  byte  signature;
  byte  version;
  char  config_wifi_ssid[SSID_MAX_LEN];
  char  config_wifi_pass[SSID_MAX_LEN];
  char  wifi_ssid[SSID_MAX_LEN];
  char  wifi_pass[PASS_MAX_LEN];
  char  api_url[URL_MAX_LEN];
  char  config_url[URL_MAX_LEN];
};

struct device_config cfg;

/* Current device mode */
#define MODE_INVALID 0
#define MODE_CONFIG 1
#define MODE_RUNNING 2
byte mode = MODE_INVALID;

/* WiFi-related configuration */
#define DEFAULT_WIFI_CONFIG_AP_SSID "RFIDCFG"
#define DEFAULT_WIFI_CONFIG_AP_PASS "cfg"

ESP8266WebServer webserver(80);

LiquidCrystal_I2C lcd(0x27, 20, 4);
const byte LCD_LINE_LEN = 20;

const byte rfid_buffer_size = 38;
byte rfid_buffer[rfid_buffer_size];

byte cmd_get_snr[] = { 0x02, 0x00, 0x03, 0x25, 0x26, 0x00, 0x00, 0x03 };
byte cmd_anticoll_a[] = { 0x02, 0x00, 0x01, 0x04, 0x05, 0x03 };

bool relay_open = false;
bool readCard = false;
String last_uid;
uint32_t time_relay_open = 0;
uint32_t last_api_send_millis = 0;

/* ******************************************************************** EEPROM configuration functions */

void read_eeprom_config(struct device_config *c) {
  byte *buf = (byte*) c;
  for (int i = 0; i < sizeof(*c); i++) {
    *(buf++) = EEPROM.read(i);
  }
  if (c->signature != CONFIG_SIGNATURE || c->version != CONFIG_VERSION) {
    memset(c, 0, sizeof(*c));
    // Initialize some defaults
    byte mac[6];
    WiFi.macAddress(mac);

    byte keybuf[] = "Random Password!";
    if (xxtea_setup(keybuf, strlen((char*)keybuf)) != XXTEA_STATUS_SUCCESS) {
      lcd.println("XXTEA key barfed");
    }

    byte ebuf[16];
    size_t ebuf_len = 16;
    if (xxtea_encrypt(mac, sizeof(mac), ebuf, &ebuf_len) != XXTEA_STATUS_SUCCESS) {
      lcd.println("XXTEA encrypt barfed");
    }
    
    strcpy(cfg.config_wifi_ssid, DEFAULT_WIFI_CONFIG_AP_SSID);
    strcat(cfg.config_wifi_ssid, "-");
    for (int i = 0; i < 3; i++) {
      char buf[3];
      sprintf(buf, "%02x", ebuf[i]);
      strcat(cfg.config_wifi_ssid, buf);
    }
    strcpy(cfg.config_wifi_pass, DEFAULT_WIFI_CONFIG_AP_PASS);
    for (int i = 3; i < 6; i++) {
      char buf[3];
      sprintf(buf, "%x", ebuf[i]);
      strcat(cfg.config_wifi_pass, buf);
    }
  }
}

void write_eeprom_config(struct device_config *c) {
  c->signature = CONFIG_SIGNATURE;
  c->version = CONFIG_VERSION;
  byte *buf = (byte*) c;
  for (int i = 0; i < sizeof(*c); i++) {
    EEPROM.write(i, *(buf++));
  }
  EEPROM.commit();
}


/* ******************************************************************** LCD support functions */

void clear_lcd_line(byte y) {
  char line[LCD_LINE_LEN+1];
  memset(line, ' ', LCD_LINE_LEN);
  line[LCD_LINE_LEN] = '\0';
  
  lcd.setCursor(0, y);
  lcd.print(line);
}

void show_lcd_spinner(byte y, char dot = '.') {  
  char spline[LCD_LINE_LEN+1];
  memset(spline, ' ', LCD_LINE_LEN);
  spline[LCD_LINE_LEN] = '\0';
  
  static signed char dot_pos = 0;
  static byte dot_direction = 0;
  static signed char dot_y = -1;

  if (dot_y != y) {
    dot_y = y;
    dot_pos = 0;
  }
  spline[dot_pos] = dot;

  if (dot_direction == 0) {
    dot_pos++;
    if (dot_pos >= LCD_LINE_LEN) {
      dot_direction = 1;
      dot_pos--;
    }
  } else {
    dot_pos--;
    if (dot_pos <= 0) {
      dot_direction = 0;
      dot_pos++;
    }
  }
  lcd.setCursor(0, y);
  lcd.print(spline);
}

/* ******************************************************************** Web server support functions */

void config_web_server() {
  webserver.on("/", http_root);
  webserver.on("/savesettings", http_save_settings);
  webserver.begin();
}

// Runs the configuration access point - with a default username and password 
void config_ap_web_server() {
  ledOn();  
  WiFi.softAP(cfg.config_wifi_ssid, cfg.config_wifi_pass);
  IPAddress a = WiFi.softAPIP();

  lcd.clear();
  lcd.print("SSID: ");
  lcd.print(cfg.config_wifi_ssid);
  lcd.setCursor(0, 1);
  lcd.print("Pass: ");
  lcd.print(cfg.config_wifi_pass);
  lcd.setCursor(0, 2);
  lcd.print("IP: ");
  lcd.print(a);

  config_web_server();
}

void config_plain_web_server() {
  ledOff();
  lcd.clear();
  lcd.setCursor(0, 2);
  lcd.print("WiFi: ");
  lcd.print(cfg.wifi_ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(cfg.wifi_ssid, cfg.wifi_pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    show_lcd_spinner(1, '?');
  }
  lcd.setCursor(0, 3);
  lcd.print("IP: ");
  IPAddress ip = WiFi.localIP();
  lcd.print(String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]));
  
  clear_lcd_line(1);
  lcd.setCursor(0, 0);
  lcd.print("USER: ");
  lcd.print(DEFAULT_WIFI_CONFIG_AP_SSID);
  lcd.setCursor(0, 1);
  lcd.print("PASS: ");
  lcd.print(cfg.config_wifi_pass);
  
  config_web_server();
  
  delay(2000);
  lcd.clear();
}

/* ******************************************************************** Remote API functions */
void handle_card_uid(byte *buf) {
  // Length is asuumed to be 4
  if (millis() - last_api_send_millis < 2000)
    return;

  char hex_buf[10];
  sprintf(hex_buf, "%02x%02x%02x%02x", buf[0], buf[1], buf[2], buf[3]);
  String hex_uid((char*)hex_buf);

  lcd.setCursor(0, 0);
  lcd.print("CARD: ");
  lcd.print(hex_uid);
  
  String url(cfg.api_url);
  url.replace("%u", hex_uid);
  
  HTTPClient http;
  http.begin(url);
  int http_code = http.GET();
  if (http_code > 0) {
    lcd.clear();
    if (http_code == HTTP_CODE_OK) {
      String response = http.getString();

      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json = jsonBuffer.parseObject(response);

      bool ok = json["ok"];

      if (ok) {
        bool door = json["door"];
        String msg = json["msg"];
        String name = json["name"];
  
        lcd.setCursor(0, 0);
        lcd.print(name);
        lcd.setCursor(0, 1);
        lcd.print(msg);
        
        if (door) {
          lcd.setCursor(0, 3);
          lcd.print("DOOR OPEN");
          startRelay();
        }
      } else {
        String msg = json["msg"];   

        lcd.setCursor(0, 0);
        lcd.print("ERROR");
        lcd.setCursor(0, 1);
        lcd.print(msg);
        delay(1000);
        lcd.clear();
      }
      delay(2000);
    } else {
      lcd.setCursor(0, 1);
      lcd.print("HTTP: " + String(http_code));
      beep(100, 2);
      delay(2000);
    }
  } else {
    lcd.setCursor(0, 1);
    lcd.print(url);
    beep(100, 1);
    delay(2000);
  }
  http.end();

  last_api_send_millis = millis();
}


void setup() {
  pinMode(PIN_LED, OUTPUT);
  ledOn();
  pinMode(PIN_BUZZER, OUTPUT);
  beep(100, 2);
  pinMode(PIN_RELAY, OUTPUT);
  pinMode(PIN_BUTTON, INPUT_PULLUP);
  Serial.begin(9600); // 9600 baud for talking to the RFID module
  lcd.begin(4, 5);
  lcd.clear();
  lcd.backlight();
  relayOff();

  lcd.setCursor(0, 0);
  lcd.print("Booting...");

  bool button = digitalRead(PIN_BUTTON) == LOW;
  lcd.setCursor(0, 1);
  lcd.print("BUTTON: ");
  lcd.print(button ? "YES" : "NO");

  EEPROM.begin(512);
  read_eeprom_config(&cfg);

  lcd.setCursor(0, 2);
  lcd.print("SSID: ");
  lcd.print(String(cfg.wifi_ssid));
  /*
  lcd.setCursor(0, 3);
  lcd.print("URL: ");
  lcd.print(String(cfg.api_url));
  */

  delay(1000);
  
  if (button || strlen(cfg.wifi_ssid) == 0 || strlen(cfg.api_url) == 0) {
    mode = MODE_CONFIG;
    config_ap_web_server();
  } else {
    mode = MODE_RUNNING;
    config_plain_web_server();
  }
  
  ledOff();
}

void loop() {
  if (mode == MODE_CONFIG || mode == MODE_RUNNING) {
    webserver.handleClient();
  }

  if (mode == MODE_RUNNING) {  
    Serial.write(cmd_get_snr, sizeof(cmd_get_snr));
    byte len = recvRfid(rfid_buffer, rfid_buffer_size);

    yield();
    
    if (len >= 5 && !relay_open) {
      lcd.clear();
      byte status = rfid_buffer[0];
      if (status == 0) {
        handle_card_uid(&(rfid_buffer[2]));
      }
    } else if (len > 0) {
      if (len == 2 && !relay_open) {
        lcd.setCursor(0, 2);
        lcd.print("NO CARD"); 
      }
    }
  }
  
  handleRelay();
  delay(250);
}


void beep(int tm, byte count) {
  for (byte i = 0; i < count; i++) {
    digitalWrite(PIN_BUZZER, HIGH);
    delay(tm);
    digitalWrite(PIN_BUZZER, LOW);
    delay(tm);  
  }
}

byte recvRfid(byte *dest, byte dest_size) {
  int rb = Serial.read();
  if (rb == -1) {
    //beep(50, 1);
    return 0;
  }
  byte b = rb;
  if (b != 0x02) {
    //beep(100, 1);
    lcd.setCursor(0,3);
    lcd.print("R:" + String(rb, 16));
    return 0;
  }
  byte station_id = Serial.read();
  byte len = Serial.read();
  for (byte i = 0; i < len; i++)
    dest[i] = Serial.read();

  byte ck = station_id ^ len;
  for (byte i = 0; i < len; i++)
    ck ^= dest[i];

  b = Serial.read(); // checksum
  if (ck != b) {
    //beep(200, 1);
    lcd.setCursor(0,3);
    //lcd.print("CX: " + String(ck, 16) + " C: " + String(b, 16));
    //return 0;
  }
  
  b = Serial.read();

  return len;
}

/* ******************************************************************** External device functions */

void ledOn() {
  digitalWrite(PIN_LED, LOW);
}

void ledOff() {
  digitalWrite(PIN_LED, HIGH);
}

void relayOff() {
  digitalWrite(PIN_RELAY, LOW); 
}

void relayOn() {
    beep(100, 1);
    digitalWrite(PIN_RELAY, HIGH);
    delay(100); 
}

void handleRelay() {
  if (relay_open) {
    relayOn();
    if (millis() - time_relay_open >= RELAY_TIME) {
      relayOff();
      relay_open = false;
      lcd.clear();
    }
  } else {
    relayOff();
  }
}

void startRelay() {
  relay_open = true;
  time_relay_open = millis();
  handleRelay();
}


/* ******************************************************************** HTML */

String get_html_head(String title) {
  return "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n<meta charset=\"utf-8\">\n<meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n<title>" + title + "</title>\n" \
  "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" \
  "<style>\n" \
  " body { font-family: sans-serif; max-width: 1000px; margin-left: auto; margin-right: auto; }\n" \
  "</style>\n" \
  "</head>\n<body>\n";
}

String get_html_foot() {
  return "</body>\n</html>\n";
}

/* ******************************************************************** Web server routes / HTTP handlers */

void http_root() {
  if (mode == MODE_RUNNING) {
    if (!webserver.authenticate(DEFAULT_WIFI_CONFIG_AP_SSID, cfg.config_wifi_pass)) {
      webserver.requestAuthentication();
      return;
    }
  }
  
  String html = get_html_head("WiFi RFID setup");
  html += "<h1>RFID device configuration</h1>\n";
  html += "<p>Here you can configure some RFID card reader settings. If you don't know what you're doing, consult an expert.</p>\n";
  html += "<form method=\"POST\" action=\"/savesettings\">\n";
  html += "<table align=\"center\"><tr><td>WiFi SSID to connect to:</td><td><input name=\"ssid\" type=\"text\"";
  if (cfg.wifi_ssid[0] != '\0')
    html += " value=\"" + String(cfg.wifi_ssid) + "\"";
  html += "></td></tr>\n";
  html += "<tr><td>WiFi password:</td><td><input name=\"pass\" type=\"password\"></td></tr>\n";
  html += "<tr><td>RFID API URL:</td><td><input name=\"api_url\" type=\"text\" size=\"50\"";
  if (cfg.api_url[0] != '\0')
    html += " value=\"" + String(cfg.api_url) + "\"";
  html += "></td></tr>\n";
  html += "<tr><td>Config API URL:</td><td><input name=\"config_url\" type=\"text\" size=\"50\"";
  if (cfg.config_url[0] != '\0')
    html += " value=\"" + String(cfg.config_url) + "\"";
  html += "></td></tr>\n";
  html += "<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\"></td></tr>\n";
  html += get_html_foot();
  webserver.send(200, "text/html", html);
}

void http_save_settings() {
  if (mode == MODE_RUNNING) {
    if (!webserver.authenticate(DEFAULT_WIFI_CONFIG_AP_SSID, cfg.config_wifi_pass)) {
      webserver.requestAuthentication();
      return;
    }
  }
  
  String html = get_html_head("WiFi RFID setup");
  html += "<p>Configuring for SSID: \"" + webserver.arg("ssid") + "\" and API URL: \"" + webserver.arg("api_url") + "\".</p>\n";

  strlcpy(cfg.wifi_ssid, webserver.arg("ssid").c_str(), sizeof(cfg.wifi_ssid));
  if (webserver.arg("pass").length() != 0)
    strlcpy(cfg.wifi_pass, webserver.arg("pass").c_str(), sizeof(cfg.wifi_pass));
  strlcpy(cfg.api_url, webserver.arg("api_url").c_str(), sizeof(cfg.api_url));
  strlcpy(cfg.config_url, webserver.arg("config_url").c_str(), sizeof(cfg.config_url));
  write_eeprom_config(&cfg);

  html += "<p>The device will now reboot.</p>";
  
  html += get_html_foot();
  webserver.send(200, "text/html", html);
  lcd.clear();
  lcd.print("Saved settings.");
  lcd.setCursor(0,1);
  lcd.print("Rebooting...");
  
  delay(1000);
  ESP.restart();
}

